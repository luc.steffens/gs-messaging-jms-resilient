package hello;

import be.acerta.common.api.plugins.jmslistener.ResilientJmsClient;
import be.acerta.common.api.plugins.jmslistener.ResilientJmsListener;
import be.acerta.common.api.plugins.jmslistener.exception.WaitAndHoldException;
import java.util.Random;
import org.springframework.stereotype.Component;

@Component
public class Receiver implements ResilientJmsClient {

    @ResilientJmsListener(destination = "mailbox")
    public void receiveResilientMessage(Email email) {
        System.out.println(">>>>> Received <" + email + ">");
    }

    @ResilientJmsListener(destination = "mailbox3")
    public void receiveResilientMessageSometimesFailing(Email email) {
        if (new Random().nextBoolean()) {
            throw new WaitAndHoldException();
        } else {
            System.out.println(">>>>> Received failing <" + email + ">");
        }
    }
}
