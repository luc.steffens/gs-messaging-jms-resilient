package hello;

import be.acerta.common.plugins.jmslistener.config.EnableJmListenerPlugin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jms.core.JmsTemplate;

@SpringBootApplication
@EnableJmListenerPlugin
public class Application {

    public static void main(String[] args) throws Exception {
        // Launch the application
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);

        // Send a message with a POJO - the template reuse the message converter
        for (int i = 1; i <= 5; i++) {
            System.out.println("Sending an email message: " );
            JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);
            jmsTemplate.convertAndSend("mailbox", new Email("m@example.com", "Hello " + i ));
            jmsTemplate.convertAndSend("mailbox3", new Email("m3@example.com", "Hello " + i));

        }
        Thread.sleep(5000);
        System.exit(0);
    }

}
