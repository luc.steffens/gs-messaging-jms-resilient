package be.acerta.common.api.plugins.jmslistener;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = {ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ResilientJmsListener {

    public String id() default "";

    public String containerFactory() default "";

    public String destination();

    public String subscription() default "";

    public String selector() default "";

    public String concurrency() default "";    
}
