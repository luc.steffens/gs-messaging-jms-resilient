package be.acerta.common.plugins.jmslistener.config;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;


@Component
@Retention(RetentionPolicy.RUNTIME)
@Import(JmListenerPluginConfig.class)
public @interface EnableJmListenerPlugin {
    
}
