package be.acerta.common.plugins.jmslistener.internal;

import be.acerta.common.api.plugins.jmslistener.ResilientJmsClient;
import be.acerta.common.api.plugins.jmslistener.ResilientJmsListener;
import be.acerta.common.api.plugins.jmslistener.exception.WaitAndHoldException;
import hello.Email;
import java.lang.reflect.InvocationTargetException;
import static java.util.Arrays.stream;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.connection.JmsTransactionManager;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.listener.SessionAwareMessageListener;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;

@Configuration
public class JmsClientScanner {

    @Autowired
    private AnnotationConfigApplicationContext context;

    @Autowired
    public List<ResilientJmsClient> jmsClients = new LinkedList<>();

    @Autowired
    public ConnectionFactory connectionFactory;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private MessageConverter jacksonJmsMessageConverter;

    @PostConstruct
    public void postConstruct() {

        for (ResilientJmsClient jmsClient : jmsClients) {
            stream(jmsClient.getClass().getMethods())
                    .filter(m -> m.isAnnotationPresent(ResilientJmsListener.class))
                    .forEach(m -> {
                        ResilientJmsListener listener = m.getDeclaredAnnotation(ResilientJmsListener.class);
                        System.out.println("listener: " + listener.destination());
                        context.registerBean(listener.destination()     ,
                                DefaultMessageListenerContainer.class,
                                () -> create(listener.destination(), msg -> {
                                    try {
                                        m.invoke(jmsClient, jacksonJmsMessageConverter.fromMessage(msg));
                                    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | JMSException | MessageConversionException ex) {
                                        throw (RuntimeException) ex.getCause();
                                        
                                    }
                                }));

                    });
        }
//        context.registerBean("mailbox3", DefaultMessageListenerContainer.class, () -> create("mailbox3", m -> {
//            if (new Random().nextBoolean()) {
//                throw new WaitAndHoldException();
//            } else {
//                System.out.println("BODY 3: " + m.toString());
//            }
//        }));

    }

    public DefaultMessageListenerContainer create(String destination, Consumer<Message> handler) {
        DefaultMessageListenerContainer container = new DefaultMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setSessionTransacted(true);
        container.setTransactionManager(new JmsTransactionManager(connectionFactory));
        container.setDestinationName(destination);
        container.setMessageListener(getListener(handler, container));
        return container;
    }

    public SessionAwareMessageListener<Message> getListener(Consumer<Message> handler, DefaultMessageListenerContainer container) {
        return (m, s) -> {
            try {
                handler.accept(m);
                m.acknowledge();
            } catch (WaitAndHoldException e) {
                s.rollback();
                container.stop();
                try {
                    Thread.sleep(500);

                } catch (InterruptedException ex) {
                    Logger.getLogger(JmsClientScanner.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
                System.out.println("!!!! Waited a bit, lets start over again !!!");
                container.start();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                s.commit();
//                System.out.println("Committing");
            }
        };
    }
}
